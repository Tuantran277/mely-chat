package com.example.chatapp.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.chatapp.Fragment.ChatsFragment;
import com.example.chatapp.Fragment.ContactsFragment;
import com.example.chatapp.Fragment.GroupsFragment;
import com.example.chatapp.Fragment.RequestFragment;

public class TabsAccessorAdapter extends FragmentPagerAdapter {

    public TabsAccessorAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:

                ChatsFragment chatsFragment = new ChatsFragment();
                return chatsFragment;
            case 1:
                GroupsFragment groupsFragment = new GroupsFragment();
                return groupsFragment;
            case 2:
                ContactsFragment contactsFragment = new ContactsFragment();
                return contactsFragment;
            case 3:
                RequestFragment requestFragment =new RequestFragment();
                return requestFragment;
            /*case 4:
                NotifyFragment notifyFragment =new NotifyFragment();
                return notifyFragment;*/
            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return 4;
    }


/*
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Chats";
            case 1:
                return "Groups";
            case 2:
                return "Contacts";
            case 3:
                return "Requests";
            case 4:
                return "Notifications";
            default:
                return null;
        }*/
    }

