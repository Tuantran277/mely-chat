package com.example.chatapp.Adapter;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Activity.ProfileActivity;
import com.example.chatapp.Model.Messages;
import com.example.melychat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupMessageAdapter extends RecyclerView.Adapter<GroupMessageAdapter.MessageViewHolder> {
    private List<Messages> groupMessageList;
    private FirebaseAuth mAuth;
    private String currentUserID;
    private DatabaseReference userRef,userRef1;



    public GroupMessageAdapter (List<Messages> groupMessageList)
    {
        this.groupMessageList = groupMessageList;
    }




    public  class MessageViewHolder extends RecyclerView.ViewHolder{

        public TextView senderMessageText, receiverMessageText,timeMessage;
        public TextView senderUserNameText,receiverUserNameText;
        public CircleImageView receiverProfileImage,senderProfileImage;

        public ImageView senderImage, receiverImage;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);

            senderMessageText = (TextView) itemView.findViewById(R.id.sender_message_text);
            receiverMessageText = (TextView) itemView.findViewById(R.id.receiver_messsage_text);
            timeMessage = (TextView) itemView.findViewById(R.id.time_message);
            receiverProfileImage =(CircleImageView) itemView.findViewById(R.id.receiver_profile_image);
            senderProfileImage =(CircleImageView) itemView.findViewById(R.id.sender_profile_image);
            senderImage = (ImageView)  itemView.findViewById(R.id.image_sender);
            receiverImage = (ImageView)  itemView.findViewById(R.id.image_receiver);
            senderUserNameText =(TextView) itemView.findViewById(R.id.sender_name_text);
            receiverUserNameText =(TextView) itemView.findViewById(R.id.receiver_name_text);
        }
    }
    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_group_message_layout,parent,false);

        mAuth= FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        return new MessageViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder holder,  int position) {

        final int adapterPosition = holder.getAdapterPosition();
        String messageSenderID = mAuth.getCurrentUser().getUid();
        Messages messages = groupMessageList.get(position);

        String fromUserID = messages.getFrom();
        String messageType = messages.getType();
        holder.receiverProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent profileIntent = new Intent(holder.itemView.getContext(), ProfileActivity.class);
                profileIntent.putExtra("receiverID", messages.getFrom());
                holder.itemView.getContext().startActivity(profileIntent);


            }
        });


        userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChild("image")) {
                    String receiverImage = snapshot.child("image").getValue().toString();
                    Picasso.get().load(receiverImage).placeholder(R.drawable.profile_image).into(holder.receiverProfileImage);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        userRef1 = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID);
        userRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChild("image")) {
                    String senderImage = snapshot.child("image").getValue().toString();
                    Picasso.get().load(senderImage).placeholder(R.drawable.profile_image).into(holder.senderProfileImage);
                    String senderName = snapshot.child("name").getValue().toString();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        holder.timeMessage.setVisibility(View.GONE);

        holder.receiverProfileImage.setVisibility(View.GONE);
        holder.senderProfileImage.setVisibility(View.GONE);
        holder.senderMessageText.setVisibility(View.GONE);
        holder.receiverImage.setVisibility(View.GONE);
        holder.senderImage.setVisibility(View.GONE);
        holder.receiverUserNameText.setVisibility(View.GONE);
        holder.senderUserNameText.setVisibility(View.GONE);

        if (messageType.equals("text")) {
            if (fromUserID.equals(messageSenderID)) {

                holder.receiverMessageText.setVisibility(View.INVISIBLE);
                holder.senderMessageText.setVisibility(View.VISIBLE);
                holder.senderMessageText.setBackgroundResource(R.drawable.sender_message_layout);
                holder.senderMessageText.setTextColor(Color.BLACK);
                if(messages.getMessage().equals("Message was deleted")){
                    holder.senderMessageText.setTextColor(Color.GRAY);
                    holder.senderMessageText.setTypeface(null,Typeface.ITALIC);
                }
                else {
                    holder.senderMessageText.setTextColor(Color.BLACK);
                }
                holder.senderMessageText.setText(messages.getMessage());
                holder.senderProfileImage.setVisibility(View.VISIBLE);
                holder.senderMessageText.setText(messages.getMessage());
                holder.senderUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String senderName = snapshot.getValue().toString();
                        holder.senderUserNameText.setText(senderName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                holder.senderMessageText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setVisibility(View.VISIBLE);
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());
                    }
                });


            } else
            {
                holder.receiverUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String receiverName = snapshot.getValue().toString();
                        holder.receiverUserNameText.setText(receiverName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                holder.receiverUserNameText.setVisibility(View.VISIBLE);
                holder.senderMessageText.setVisibility(View.INVISIBLE);
                holder.senderProfileImage.setVisibility(View.INVISIBLE);
                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.receiverMessageText.setVisibility(View.VISIBLE);

                holder.receiverMessageText.setBackgroundResource(R.drawable.receiver_message_layout);
                holder.receiverMessageText.setTextColor(Color.BLACK);

                if(messages.getMessage().equals("Message was deleted"))
                {
                    holder.receiverMessageText.setTextColor(Color.CYAN);
                }
                else
                {
                    holder.receiverMessageText.setTextColor(Color.BLACK);
                    }
                holder.receiverMessageText.setText(messages.getMessage());
                holder.receiverMessageText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setVisibility(View.VISIBLE);
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());
                    }
                });

            }

            holder.timeMessage.setVisibility(View.VISIBLE);
            holder.timeMessage.setText(messages.getDate());

        }

        else if (messageType.equals("image")) {
            holder.receiverMessageText.setVisibility(View.INVISIBLE);
            holder.senderMessageText.setVisibility(View.INVISIBLE);
            //holder.senderProfileImage.setVisibility(View.INVISIBLE);
            //holder.receiverProfileImage.setVisibility(View.INVISIBLE);
            holder.senderImage.setVisibility(View.INVISIBLE);
            //holder.receiverImage.setVisibility(View.INVISIBLE);

            holder.timeMessage.setVisibility(View.VISIBLE);
            if (fromUserID.equals(messageSenderID)) {
                holder.receiverImage.setVisibility(View.INVISIBLE);
                holder.receiverProfileImage.setVisibility(View.INVISIBLE);
                //holder.receiverMessageText.setVisibility(View.INVISIBLE);
                Picasso.get().load(messages.getMessage()).transform(transformation).into(holder.senderImage);
                holder.senderImage.setVisibility(View.VISIBLE);
                holder.senderProfileImage.setVisibility(View.VISIBLE);
                holder.timeMessage.setVisibility(View.VISIBLE);
                holder.timeMessage.setText(messages.getDate());

                holder.senderUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String senderName = snapshot.getValue().toString();
                        holder.senderUserNameText.setText(senderName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

                holder.senderImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());
                        Dialog dialog = new Dialog(v.getContext());
                        dialog.setContentView(R.layout.dialog_image); // Layout của Dialog, chứa ImageView để hiển thị ảnh

                        ImageView imageView = dialog.findViewById(R.id.dialogImageView);
                        // Load ảnh và hiển thị trong ImageView
                        Picasso.get().load(messages.getMessage()).into(imageView);

                        // Định dạng kích thước cho Dialog
                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        layoutParams.copyFrom(dialog.getWindow().getAttributes());
                        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                        dialog.getWindow().setAttributes(layoutParams);

                        dialog.show();
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());
                        ImageButton backButton = dialog.findViewById(R.id.dialogBackButton);
                        backButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss(); // Đóng Dialog khi ấn nút back
                            }
                        });
                    }
                });
            }
            else {
                holder.receiverUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String receiverName = snapshot.getValue().toString();
                        holder.receiverUserNameText.setText(receiverName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                holder.senderImage.setVisibility(View.INVISIBLE);

                holder.senderProfileImage.setVisibility(View.INVISIBLE);
                Picasso.get().load(messages.getMessage()).transform(transformation).into(holder.receiverImage);

                holder.timeMessage.setVisibility(View.VISIBLE);
                holder.timeMessage.setText(messages.getDate());
                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.receiverImage.setVisibility(View.VISIBLE);

                holder.receiverImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());

                        Dialog dialog = new Dialog(v.getContext());
                        dialog.setContentView(R.layout.dialog_image); // Layout của Dialog, chứa ImageView để hiển thị ảnh

                        ImageView imageView = dialog.findViewById(R.id.dialogImageView);
                        // Load ảnh và hiển thị trong ImageView
                        Picasso.get().load(messages.getMessage()).into(imageView);

                        // Định dạng kích thước cho Dialog
                        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                        layoutParams.copyFrom(dialog.getWindow().getAttributes());
                        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                        dialog.getWindow().setAttributes(layoutParams);

                        dialog.show();
                        holder.timeMessage.setText(messages.getTime() + "  " + messages.getDate());
                        ImageButton backButton = dialog.findViewById(R.id.dialogBackButton);
                        backButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss(); // Đóng Dialog khi ấn nút back
                            }
                        });
                    }
                });
            }

        }

        else if(messageType.equals("pdf") || messageType.equals("docx")){

            holder.senderImage.setVisibility(View.INVISIBLE);
            holder.receiverImage.setVisibility(View.INVISIBLE);

            holder.timeMessage.setVisibility(View.VISIBLE);
            if(fromUserID.equals(messageSenderID)){
                holder.receiverProfileImage.setVisibility(View.INVISIBLE);
                holder.timeMessage.setText(messages.getDate());
                holder.receiverMessageText.setVisibility(View.INVISIBLE);
                holder.senderMessageText.setVisibility(View.VISIBLE);
                holder.senderMessageText.setTextColor(Color.BLUE);
                holder.senderMessageText.setTypeface(null, Typeface.BOLD_ITALIC);
                holder.senderMessageText.setText("Sent an attachment file");
                holder.senderProfileImage.setVisibility(View.VISIBLE);

                holder.timeMessage.setVisibility(View.VISIBLE);

                holder.senderUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String senderName = snapshot.getValue().toString();
                        holder.senderUserNameText.setText(senderName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                holder.senderMessageText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setText(messages.getTime()+"  "+messages.getDate());
                    }
                });


            } else {

                holder.senderImage.setVisibility(View.INVISIBLE);
                holder.senderProfileImage.setVisibility(View.INVISIBLE);

                holder.receiverProfileImage.setVisibility(View.VISIBLE);
                holder.receiverMessageText.setTextColor(Color.BLUE);
                holder.receiverMessageText.setVisibility(View.VISIBLE);
                holder.receiverMessageText.setTypeface(null, Typeface.BOLD_ITALIC);
                holder.receiverMessageText.setText("Sent an attachment file");

                holder.receiverUserNameText.setVisibility(View.VISIBLE);
                userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID).child("name");
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        String receiverName = snapshot.getValue().toString();
                        holder.receiverUserNameText.setText(receiverName);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                holder.receiverMessageText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timeMessage.setText(messages.getTime()+"  "+messages.getDate());
                    }
                });





            }
        }

        if(fromUserID.equals(messageSenderID)){

            if(groupMessageList.get(adapterPosition).getType().equals("pdf")||groupMessageList.get(adapterPosition).getType().equals("docx")) {

                holder.senderMessageText.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        CharSequence option[] = new CharSequence[]{
                                "Delete",
                                "Download",
                                "Cancel"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
                        builder.setTitle("Select action: ");
                        builder.setItems(option, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    deleteFileMessages(adapterPosition,holder);
                                    holder.senderMessageText.setTextColor(Color.GRAY);
                                    holder.senderMessageText.setText("Message was deleted");
                                }
                                if (which == 1) {

                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(groupMessageList.get(adapterPosition).getMessage()));
                                    holder.itemView.getContext().startActivity(intent);

                                } else if (which == 2) {

                                }
                            }
                        });
                        builder.show();
                        return true;
                    }
                });
            }
            else if (groupMessageList.get(adapterPosition).getType().equals("text")) {
                if(!messages.getMessage().equals("Message was deleted"))
                {
                    holder.senderMessageText.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            CharSequence option[] = new CharSequence[]{
                                    "Delete",
                                    "Cancel"
                            };
                            AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
                            builder.setTitle("Select action: ");
                            builder.setItems(option, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (which == 0) {
                                        //holder.senderMessageText.setVisibility(View.VISIBLE);
                                        holder.senderMessageText.setTextColor(Color.GRAY);
                                        deleteTextSentMessages(adapterPosition, holder);
                                        holder.senderMessageText.setText("Message was deleted");

                                    } else if (which == 1) {

                                    }
                                }
                            });
                            builder.show();
                            return true;
                        }
                    });

                }
            }
            else if (groupMessageList.get(adapterPosition).getType().equals("image")) {
                holder.senderImage.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override public boolean onLongClick(View v) {
                        CharSequence option[] = new CharSequence[]{
                                "Delete",
                                "DownLoad",
                                "Cancel"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
                        builder.setTitle("Select action:");
                        builder.setItems(option, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (which == 0) {
                                    holder.senderImage.setVisibility(View.INVISIBLE);
                                    deleteImageSentMessages(adapterPosition,holder);
                                    holder.senderMessageText.setVisibility(View.VISIBLE);
                                    holder.senderMessageText.setTextColor(Color.GRAY);
                                    holder.senderMessageText.setText("Message was deleted");

                                }
                                 if (which == 1) {

                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(groupMessageList.get(adapterPosition).getMessage()));
                                    holder.itemView.getContext().startActivity(intent);
                                }
                            }
                        });
                        builder.show();
                        return true;
                    }
                });

            }

        }
        else
        {

            if(groupMessageList.get(adapterPosition).getType().equals("pdf") || groupMessageList.get(adapterPosition).getType().equals("docx")) {

                holder.receiverMessageText.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        CharSequence options[] = new CharSequence[]
                                {
                                        "Download",
                                        "Cancel"
                                };
                        AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
                        builder.setTitle("Select action");

                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(groupMessageList.get(adapterPosition).getMessage()));
                                    holder.itemView.getContext().startActivity(intent);

                                } else if (which == 1) {

                                }
                            }
                        });
                        builder.show();
                        return true;
                    }
                });

            }
            else if (groupMessageList.get(adapterPosition).getType().equals("text"))
            {


            }
            else if(groupMessageList.get(adapterPosition).getType().equals("image")){

                holder.receiverImage.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        CharSequence options[] = new CharSequence[]
                                {
                                        "Download",
                                        "Cancel"
                                };
                        AlertDialog.Builder builder = new AlertDialog.Builder(holder.itemView.getContext());
                        builder.setTitle("Select action");

                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == 0){
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(groupMessageList.get(adapterPosition).getMessage()));
                                    holder.itemView.getContext().startActivity(intent);
                                }
                            }
                        });
                        builder.show();
                        return true;
                    }
                });


            }

        }



    }

    @Override
    public int getItemCount()
    {
        return groupMessageList.size();
    }



    private void deleteTextSentMessages( final int position, final MessageViewHolder holder)
    {
        //final int adapterPosition = holder.getAdapterPosition();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                        .child(groupMessageList.get(position).getMessageID()).child("message")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String delete_message="Message was deleted";
                        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                                .child(groupMessageList.get(position).getMessageID()).child("message").setValue(delete_message);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
    private void deleteImageSentMessages( final int position, final MessageViewHolder holder)
    {
        //final int adapterPosition = holder.getAdapterPosition();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                .child(groupMessageList.get(position).getMessageID()).child("message")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String delete_message="Message was deleted";
                        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                                .child(groupMessageList.get(position).getMessageID()).child("message").setValue(delete_message);
                        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                                .child(groupMessageList.get(position).getMessageID()).child("type").setValue("text");

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
    private void deleteFileMessages( final int position, final MessageViewHolder holder)
    {
        //final int adapterPosition = holder.getAdapterPosition();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                .child(groupMessageList.get(position).getMessageID()).child("message")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String delete_message="Message was deleted";
                        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                                .child(groupMessageList.get(position).getMessageID()).child("message").setValue(delete_message);
                        rootRef.child("Groups").child(groupMessageList.get(position).getGroup()).child("Message")
                                .child(groupMessageList.get(position).getMessageID()).child("type").setValue("text");

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
    //bo goc tuy chinh
    Transformation transformation = new Transformation() {
        @Override
        public Bitmap transform(Bitmap source) {
            int radius = 10; // Bán kính bo góc
            Bitmap bitmap = Bitmap.createBitmap(source.getWidth(), source.getHeight(), source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            canvas.drawRoundRect(new RectF(0, 0, source.getWidth(), source.getHeight()), radius, radius, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(source, 0, 0, paint);
            if (source != bitmap) {
                source.recycle();
            }
            return bitmap;
        }

        @Override
        public String key() {
            return "rounded_corners";
        }
    };
}
