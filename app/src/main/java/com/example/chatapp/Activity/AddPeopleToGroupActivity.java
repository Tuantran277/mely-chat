package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatapp.FCMSend;
import com.example.melychat.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddPeopleToGroupActivity extends AppCompatActivity {

    private String senderUserID, currentUserName, Current_State, currentUserID, receiverUserID, currentGroupName;
    private CircleImageView userProfileImage;
    private TextView userProfileName, userProfileStatus;
    private Button SendMessageRequestButton, DeclineMessageRequestButton;

    private DatabaseReference UserRef, rootRef, ChatRequestRef;
    private boolean GroupAdded;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_peopleto_group);

        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();
        currentUserID = mAuth.getCurrentUser().getUid();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");

        senderUserID = mAuth.getCurrentUser().getUid();
        //Toast.makeText(this, "UserID "+receiverUserID, Toast.LENGTH_SHORT).show();

        userProfileImage = (CircleImageView) findViewById(R.id.visit_profile_image);
        userProfileName = (TextView) findViewById(R.id.visit_profile_name);
        userProfileStatus = (TextView) findViewById(R.id.visit_profile_status);
        SendMessageRequestButton = (Button) findViewById(R.id.add_to_group_button);
        Current_State = "new";


        if (getIntent().hasExtra("visit_user_id")) {
            receiverUserID = getIntent().getStringExtra("visit_user_id");
        }

        if (getIntent().hasExtra("group_name")) {
            currentGroupName = getIntent().getStringExtra("group_name");
        }
        RetrieveInfo();

        if(currentUserID.equals(receiverUserID)){
            SendMessageRequestButton.setVisibility(View.INVISIBLE);
        }
        SendMessageRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRef.child(receiverUserID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        boolean groupFound = false;
                        for (int i = 1; i < 1000; i++) {
                            String groupKey = "group" + i;
                            if (snapshot.hasChild(groupKey)) {
                                String groupName = snapshot.child(groupKey).getValue(String.class);
                                if (groupName.equals(currentGroupName)) {
                                    UserRef.child(receiverUserID).child(groupKey).removeValue();
                                    UserRef.child(receiverUserID).child("name").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            Toast.makeText(AddPeopleToGroupActivity.this, "Removed " + snapshot.getValue(String.class), Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                    //Toast.makeText(AddPeopletoGroup.this, "Removed " + c, Toast.LENGTH_SHORT).show();

                                    UserRef.child(currentUserID).child("name").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            currentUserName = snapshot.getValue(String.class);

                                            UserRef.child(receiverUserID).child("device_token").child("result").addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                    String token = snapshot.getValue(String.class);
                                                    FCMSend.pushNotification(AddPeopleToGroupActivity.this,token,"MelyChat",currentUserName+" removed you from "+currentGroupName);
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError error) {

                                                }
                                            });
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                    Intent intent = new Intent(AddPeopleToGroupActivity.this, GroupChatActivity.class);
                                    /*intent.putExtra("Add_user_name", receiverUserID);
                                    intent.putExtra("group_name",currentGroupName);
                                    startActivity(intent);*/
                                    onBackPressed();
                                    groupFound = true;
                                    break;
                                }
                            } else {
                                UserRef.child(receiverUserID).child(groupKey).setValue(currentGroupName);
                                //Toast.makeText(AddPeopletoGroup.this, "Added " + currentUserID, Toast.LENGTH_SHORT).show();
                                UserRef.child(receiverUserID).child("name").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        Toast.makeText(AddPeopleToGroupActivity.this, "Added " + snapshot.getValue(String.class), Toast.LENGTH_SHORT).show();
                                        UserRef.child(currentUserID).child("name").addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                currentUserName = snapshot.getValue(String.class);

                                                UserRef.child(receiverUserID).child("device_token").child("result").addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                        String token = snapshot.getValue(String.class);
                                                        FCMSend.pushNotification(AddPeopleToGroupActivity.this,token,"MelyChat",currentUserName+" add you to "+currentGroupName);
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError error) {

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError error) {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                                onBackPressed();
                                groupFound = true;
                                break;
                            }
                        }
                        if (!groupFound) {
                            Toast.makeText(AddPeopleToGroupActivity.this, "No group found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        // Xử lý khi có lỗi xảy ra
                    }
                });
            }
        });




    }

    public void addToGroup() {
        for (int i = 1; i < 100; i++) {
            int i1 = i;
            UserRef.child(receiverUserID).child("group" + i).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (!snapshot.exists()) {
                        UserRef.child(receiverUserID).child("group" + i1).setValue(currentGroupName);
                        Toast.makeText(AddPeopleToGroupActivity.this, "Add sucessfully", Toast.LENGTH_SHORT).show();
                        GroupAdded = true;
                        Current_State = "added";
                        SendMessageRequestButton.setText("Remove");
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
            if (GroupAdded)
                break;
        }

    }

    public void removeFromGroup() {
        for (int i = 1; i < 100; i++) {
            int i1 = i;
            UserRef.child(receiverUserID).child("group" + i).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.getValue().equals(currentGroupName)) {
                        UserRef.child(receiverUserID).child("group" + i1).removeValue();
                        Toast.makeText(AddPeopleToGroupActivity.this, "Remove sucessfully!", Toast.LENGTH_SHORT).show();
                        GroupAdded = false;
                        Current_State = "removed";
                        SendMessageRequestButton.setText("Add to group");
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void RetrieveInfo() {
        UserRef.child(receiverUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if ((snapshot.exists()) && (snapshot.hasChild("image"))) {
                    String userImage = snapshot.child("image").getValue().toString();
                    String userName = snapshot.child("name").getValue().toString();
                    String userStatus = snapshot.child("status").getValue().toString();

                    Picasso.get().load(userImage).placeholder(R.drawable.profile_image).into(userProfileImage);
                    userProfileName.setText(userName);
                    userProfileStatus.setText(userStatus);


                } else {
                    String userName = snapshot.child("name").getValue().toString();
                    String userStatus = snapshot.child("status").getValue().toString();

                    userProfileName.setText(userName);
                    userProfileStatus.setText(userStatus);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        updateUserStatus("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateUserStatus("offline");
    }

    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        rootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }
}