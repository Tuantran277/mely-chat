package com.example.chatapp.Activity;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatapp.Adapter.MessageAdapter;
import com.example.chatapp.FCMSend;
import com.example.chatapp.Fragment.ChatsFragment;
import com.example.chatapp.Model.Messages;
import com.example.melychat.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private String messageReceivedID, messageReceivedName, messageReceivedImage,messageSenderID,currentUserID;

    private String checker ="",myUrl="";


    private ArrayList<String> messageList1 = new ArrayList<>();
    private TextView userName, userLastseen;
    private FirebaseAuth mAuth;
    private CircleImageView userImage;
    private ImageButton sendChatBtn,sendFileBtn;
    private DatabaseReference RootRef,userRef;

    private Toolbar chatToolbar;
    private EditText messageInputText;
    private final List<Messages> messageList= new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;
    private RecyclerView userMessageList;
    private Uri fileUri;
    private ProgressDialog loadingBar;
    private StorageTask uploadTask;

    private String saveCurrentTime, saveCurrentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Init();

        mAuth= FirebaseAuth.getInstance();
        messageSenderID = mAuth.getCurrentUser().getUid();
        currentUserID = mAuth.getCurrentUser().getUid();
        RootRef = FirebaseDatabase.getInstance().getReference();

        userRef= FirebaseDatabase.getInstance().getReference().child("Users");
        messageReceivedID = getIntent().getExtras().get("visit_user_id").toString();
        messageReceivedName = getIntent().getExtras().get("visit_user_name").toString();
        messageReceivedImage = getIntent().getExtras().get("visit_image").toString();
        loadingBar = new ProgressDialog(this);

        //Toast.makeText(ChatActivity.this, messageReceivedName, Toast.LENGTH_SHORT).show();
        //Toast.makeText(ChatActivity.this, messageReceivedId, Toast.LENGTH_SHORT).show();


        userName.setText(messageReceivedName);
        Picasso.get().load(messageReceivedImage).placeholder(R.drawable.profile_image).into(userImage);


        sendChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        displayLastSeen();
        sendFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[]{
                            "Images file",
                        "PDF & Word file",
                        "Cancel"

                };
                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle("Choose the type file:");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0)
                        {

                            checker ="image";
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            startActivityForResult(intent.createChooser(intent,"Select a image:"),438);


                        }

                        if(which ==1){
                            checker = "docx";
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            String[] mimeTypes = {"application/msword","application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
                            intent.setType("*/*");
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            startActivityForResult(intent.createChooser(intent, "Select a file"), 438);
                        }

                    }
                });
                builder.show();
            }
        });
    }


    private void Init() {
        chatToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(chatToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");


        LayoutInflater layoutInflater =(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = layoutInflater.inflate(R.layout.custom_chat_bar, null);
        actionBar.setCustomView(actionBarView);

        userImage = (CircleImageView) findViewById(R.id.chat_profile_image);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(ChatActivity.this,ProfileActivity.class);
                profileIntent.putExtra("receiverID", messageReceivedID);
                startActivity(profileIntent);


            }
        });
        userName = (TextView) findViewById(R.id.chat_profile_name);
        userLastseen = (TextView) findViewById(R.id.custom_user_last_seen);
        sendChatBtn =(ImageButton ) findViewById(R.id.send_private_message_button);
        messageInputText= (EditText) findViewById(R.id.input_private_message);
        sendFileBtn = (ImageButton ) findViewById(R.id.send_file_btn);

        messageAdapter = new MessageAdapter(messageList);
        userMessageList = (RecyclerView) findViewById(R.id.private_message_list);
        linearLayoutManager = new LinearLayoutManager(this);
        userMessageList.setLayoutManager(linearLayoutManager);
        userMessageList.setAdapter(messageAdapter);

        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());



    }
    private void displayLastSeen(){
        RootRef.child("Users").child(messageReceivedID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("userState").hasChild("state"))
                        {
                            String state = dataSnapshot.child("userState").child("state").getValue().toString();
                            String date = dataSnapshot.child("userState").child("date").getValue().toString();
                            String time = dataSnapshot.child("userState").child("time").getValue().toString();

                            if (state.equals("online"))
                            {
                                userLastseen.setText("Online now");


                            }
                            else if (state.equals("offline"))
                            {
                                userLastseen.setText("Last online: " + time + " " +date);
                            }
                        }
                        else
                        {
                            userLastseen.setText("Offline");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 438 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            loadingBar.setTitle("Uploading File");
            loadingBar.setMessage("Please wait while uploading the file");
            loadingBar.setCanceledOnTouchOutside(false);
            loadingBar.show();

            fileUri = data.getData();
            if(!checker.equals("image"))
            {
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Document Files");

                String messageSenderRef="Message/"+ messageSenderID + "/"+ messageReceivedID;
                String messageReceivedRef="Message/"+ messageReceivedID + "/"+ messageSenderID;

                DatabaseReference userMessageKeyRef = RootRef.child("Message")
                        .child(messageSenderID).child(messageReceivedID).push();

                final String messagePushID = userMessageKeyRef.getKey();

                StorageReference filePath = storageReference.child(messagePushID+"."+ checker);
                filePath.putFile(fileUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task)
                    {
                        if(task.isSuccessful()){
                            task.getResult().getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri downloadUri) {
                                    Map messageImageBody = new HashMap();
                                    messageImageBody.put("message", downloadUri.toString());
                                    messageImageBody.put("name", fileUri.getLastPathSegment());
                                    messageImageBody.put("type", checker);
                                    messageImageBody.put("from", messageSenderID);
                                    messageImageBody.put("to", messageReceivedID);
                                    messageImageBody.put("messageID", messagePushID);
                                    messageImageBody.put("time", saveCurrentTime);
                                    messageImageBody.put("date", saveCurrentDate);

                                    Map messageBodyDetails = new HashMap();
                                    messageBodyDetails.put(messageSenderRef + "/" + messagePushID, messageImageBody);
                                    messageBodyDetails.put(messageReceivedRef + "/" + messagePushID, messageImageBody);

                                    RootRef.updateChildren(messageBodyDetails);
                                    loadingBar.dismiss();
                                    //push notification
                                    userRef.child(messageSenderID).child("name").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            String senderName = snapshot.getValue(String.class);
                                            userRef.child(messageReceivedID).child("userState").child("state").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot snapshot) {

                                                    userRef.child(messageReceivedID).child("device_token").child("result").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                            String result = snapshot.getValue(String.class);
                                                            //System.out.println("token recnnn "+result);
                                                            FCMSend.pushNotification(ChatActivity.this,
                                                                    result,
                                                                    "MeLyChat\n New message", senderName + " sent an attachment file! "
                                                            );

                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError error) {

                                                        }
                                                    });


                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError error) {

                                                }
                                            });
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });

                                }
                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingBar.dismiss();
                        Toast.makeText(ChatActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        double p =(100.0*snapshot.getBytesTransferred())/ snapshot.getTotalByteCount();
                        loadingBar.setMessage("Uploading "+(int) p+"%");
                    }
                });


            }
            else if(checker.equals("image"))
            {
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Image Files");

                String messageSenderRef="Message/"+ messageSenderID + "/"+ messageReceivedID;
                String messageReceivedRef="Message/"+ messageReceivedID + "/"+ messageSenderID;

                DatabaseReference userMessageKeyRef = RootRef.child("Message")
                        .child(messageSenderID).child(messageReceivedID).push();


                final String messagePushID = userMessageKeyRef.getKey();

                StorageReference filePath = storageReference.child(messagePushID+"."+ "jpg");

                uploadTask = filePath.putFile(fileUri);
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful())
                        {
                            Uri downloadUrl = task.getResult();
                            myUrl = downloadUrl.toString();

                            Map messageImageBody = new HashMap();
                            messageImageBody.put("message", myUrl);
                            messageImageBody.put("name", fileUri.getLastPathSegment());
                            messageImageBody.put("type", checker);
                            messageImageBody.put("from", messageSenderID);
                            messageImageBody.put("to", messageReceivedID);
                            messageImageBody.put("messageID", messagePushID);
                            messageImageBody.put("time", saveCurrentTime);
                            messageImageBody.put("date", saveCurrentDate);

                            Map messageBodyDetails = new HashMap();
                            messageBodyDetails.put(messageSenderRef + "/" + messagePushID, messageImageBody);
                            messageBodyDetails.put(messageReceivedRef + "/" + messagePushID, messageImageBody);

                            RootRef.updateChildren(messageBodyDetails);
                            loadingBar.dismiss();


                            //push notification
                            userRef.child(messageSenderID).child("name").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    String senderName = snapshot.getValue(String.class);
                                    userRef.child(messageReceivedID).child("userState").child("state").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                                            userRef.child(messageReceivedID).child("device_token").child("result").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                    String result = snapshot.getValue(String.class);
                                                    //System.out.println("token recnnn "+result);
                                                    FCMSend.pushNotification(ChatActivity.this,
                                                            result,
                                                            "MeLyChat\n New message", senderName + " sent an image! "
                                                    );

                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError error) {

                                                }
                                            });


                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingBar.dismiss();
                        Toast.makeText(ChatActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            else {
                loadingBar.dismiss();
                Toast.makeText(this, "No image file secleted. Error", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        updateUserStatus("online");
        messageList.clear();
        displayLastSeen();
        RootRef.child("Message").child(messageSenderID).child(messageReceivedID)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                        Messages messages = snapshot.getValue(Messages.class);
                        messageList.add(messages);
                        messageAdapter.notifyDataSetChanged();
                        userMessageList.smoothScrollToPosition(userMessageList.getAdapter().getItemCount());
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void sendMessage()
    {
        String messageText = messageInputText.getText().toString();

        if(TextUtils.isEmpty(messageText))
        {
            Toast.makeText(this, "Write your message first.", Toast.LENGTH_SHORT).show();
        }
        else
        {

            Intent chatIntent = new Intent(ChatActivity.this, MainActivity.class);
            chatIntent.putExtra("last message",messageText );

            userRef.child(messageSenderID).child("name").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String senderName = snapshot.getValue(String.class);
                    userRef.child(messageReceivedID).child("userState").child("state").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                                userRef.child(messageReceivedID).child("device_token").child("result").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        String result = snapshot.getValue(String.class);
                                        //System.out.println("token recnnn "+result);
                                        FCMSend.pushNotification(ChatActivity.this,
                                                result,
                                                "MeLyChat\n New message", senderName + ": " + messageText
                                        );

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });



            String messageSenderRef="Message/"+ messageSenderID + "/"+ messageReceivedID;
            String messageReceivedRef="Message/"+ messageReceivedID + "/"+ messageSenderID;

            DatabaseReference userMessageKeyRef = RootRef.child("Message")
                    .child(messageSenderID).child(messageReceivedID).push();

            String messagePushID = userMessageKeyRef.getKey();

            Map messageTextBody = new HashMap<>();
            messageTextBody.put("message",messageText);
            messageTextBody.put("type","text");
            messageTextBody.put("from",messageSenderID);
            messageTextBody.put("to",messageReceivedID);
            messageTextBody.put("messageID",messagePushID);
            messageTextBody.put("time",saveCurrentTime);
            messageTextBody.put("date",saveCurrentDate);

            Map messageBodyDetail = new HashMap();
            messageBodyDetail.put(messageSenderRef+"/"+ messagePushID, messageTextBody);
            messageBodyDetail.put(messageReceivedRef+"/"+ messagePushID, messageTextBody);
            
            RootRef.updateChildren(messageBodyDetail).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful())
                    {
                        messageList1.add(messageText);
                    }
                    else
                    {
                        Toast.makeText(ChatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                    messageInputText.setText("");
                }
            });
            
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent chatIntent = new Intent(ChatActivity.this, MainActivity.class);
            chatIntent.putExtra("receiverID", messageReceivedID);
            chatIntent.putExtra("senderID", messageSenderID);
            if (!messageList1.isEmpty()) {
                String lastMessage = messageList1.get(messageList1.size() - 1);
                chatIntent.putExtra("last message",lastMessage);
            }
            startActivity(chatIntent);
            return true;
    }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        messageList.clear();
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateUserStatus("offline");
        messageList.clear();
    }
    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        RootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }


}