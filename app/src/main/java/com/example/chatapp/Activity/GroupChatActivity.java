package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatapp.Adapter.GroupMessageAdapter;
import com.example.chatapp.Model.Messages;
import com.example.melychat.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;


public class GroupChatActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private Uri fileUri;
    private ImageButton sendMessageButton, sendFileBtn;
    private GroupMessageAdapter adapter;
    private String checker="",myUrl="";;
    private EditText messageInputText;
    private ScrollView mScrollView;
    private RelativeLayout mRelative;
    private TextView displayTextMessages, displayTimeMessage, GroupName;
    private RecyclerView groupMessageList;
    private final List<Messages> messageList= new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private FirebaseAuth mAuth;
    private DatabaseReference userRef,rootRef;
    private String saveCurrentTime, saveCurrentDate;

    private StorageTask uploadTask;

    private String currentGroupName, currentUserID;
    private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat1);
        Init();
        mAuth= FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();

        userRef= FirebaseDatabase.getInstance().getReference().child("Users");

        currentGroupName = getIntent().getExtras().get("groupName").toString();


        loadingBar = new ProgressDialog(this);

        GroupName.setText(currentGroupName);
//        GroupName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(GroupChatActivity.this, DisplayMemberGroupActivity.class);
//                intent.putExtra("current_group",currentGroupName);
//                startActivity(intent);
//            }
//        });

        rootRef.child("Groups").child(currentGroupName).child("background").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String background = snapshot.getValue(String.class);
                String winterBackgroundImageUri = "https://firebasestorage.googleapis.com/v0/b/melychat-beb22.appspot.com/o/Background%20Image%2Fbackground_winter.jpg?alt=media&token=b27be79c-daf6-4c47-b40c-d847e3fbfc18";
                String gameBackgroundImageUri = "https://firebasestorage.googleapis.com/v0/b/melychat-beb22.appspot.com/o/Background%20Image%2Fbackground_game.jpg?alt=media&token=9587ade4-e875-4aa0-9d3e-3a6850570421";
                String girlBackgroundImageUri = "https://firebasestorage.googleapis.com/v0/b/melychat-beb22.appspot.com/o/Background%20Image%2Fbackground_girl.jpg?alt=media&token=fed73190-4a99-4dd7-b614-85e857fd55a9";
                String learnBackgroundImageUri = "https://firebasestorage.googleapis.com/v0/b/melychat-beb22.appspot.com/o/Background%20Image%2Fbackground_learn.jpg?alt=media&token=dd440b0e-0f42-449b-b77d-e0e100591cf2";
                String loveBackgroundImageUri = "https://firebasestorage.googleapis.com/v0/b/melychat-beb22.appspot.com/o/Background%20Image%2Fbackground_love.jpg?alt=media&token=3b931cfb-a69e-4efe-a1d4-69c0dd70290c";

                if(background.equals("winter")){

                    Picasso.get().load(winterBackgroundImageUri).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mRelative.setBackground(new BitmapDrawable(getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            // Xử lý khi không tải được hình ảnh
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            // Chuẩn bị trước khi tải hình ảnh
                        }
                    });
                } else if (background.equals("game")) {
                    Picasso.get().load(gameBackgroundImageUri).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mRelative.setBackground(new BitmapDrawable(getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            // Xử lý khi không tải được hình ảnh
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            // Chuẩn bị trước khi tải hình ảnh
                        }
                    });
                } else if (background.equals("love")) {
                    Picasso.get().load(loveBackgroundImageUri).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mRelative.setBackground(new BitmapDrawable(getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            // Xử lý khi không tải được hình ảnh
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            // Chuẩn bị trước khi tải hình ảnh
                        }
                    });
                }
                else if(background.equals("learn")){
                    Picasso.get().load(learnBackgroundImageUri).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mRelative.setBackground(new BitmapDrawable(getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            // Xử lý khi không tải được hình ảnh
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            // Chuẩn bị trước khi tải hình ảnh
                        }
                    });
                } else if (background.equals("girl")) {
                    Picasso.get().load(girlBackgroundImageUri).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mRelative.setBackground(new BitmapDrawable(getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            // Xử lý khi không tải được hình ảnh
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            // Chuẩn bị trước khi tải hình ảnh
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        sendFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    CharSequence options[] = new CharSequence[]{
                            "Images file",
                            "PDF & Word file",
                            "Cancel"

                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(GroupChatActivity.this);
                    builder.setTitle("Choose the type file:");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(which == 0)
                            {

                                checker ="image";
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.setType("image/*");
                                startActivityForResult(intent.createChooser(intent,"Select a image:"),438);


                            }

                            if(which ==1){
                                checker = "docx";
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                String[] mimeTypes = {"application/msword","application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
                                intent.setType("*/*");
                                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                                startActivityForResult(intent.createChooser(intent, "Select a file"), 438);
                            }

                        }
                    });
                    builder.show();
                }
            });


    }


    @Override
    protected void onStart() {
        super.onStart();
        updateUserStatus("online");
        messageList.clear();

        rootRef.child("Groups").child(currentGroupName).child("Message")
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                            Messages messages = snapshot.getValue(Messages.class);
                            messageList.add(messages);
                            adapter.notifyDataSetChanged();
                            groupMessageList.smoothScrollToPosition(groupMessageList.getAdapter().getItemCount());
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

    }
    private void Init() {
        mToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle(currentGroupName);

        mRelative =(RelativeLayout) findViewById(R.id.group_chat_relative);


        LayoutInflater layoutInflater =(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = layoutInflater.inflate(R.layout.custom_group_chat_bar, null);
        actionBar.setCustomView(actionBarView);


        GroupName = (TextView) findViewById(R.id.group_chat_name_Tv);

        sendMessageButton =(ImageButton ) findViewById(R.id.send_group_message_button);
        messageInputText= (EditText) findViewById(R.id.input_group_message);
        sendFileBtn = (ImageButton ) findViewById(R.id.send_file_group_btn);

        adapter = new GroupMessageAdapter(messageList);
        groupMessageList= (RecyclerView) findViewById(R.id.group_message_list);

        linearLayoutManager = new LinearLayoutManager(this);
        groupMessageList.setLayoutManager(linearLayoutManager);
        groupMessageList.setAdapter(adapter);

        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menu2, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if(item.getItemId() ==R.id.add_people){
            Intent intent = new Intent(GroupChatActivity.this, FindPeopleGroup.class);
            intent.putExtra("group_name",currentGroupName);
            startActivity(intent);
        }


        if (item.getItemId() == R.id.exitgroup) {
            final AtomicBoolean created = new AtomicBoolean(false);
            for (int i = 1; i < 1000; i++) {
                int i1 = i;
                rootRef.child("Users").child(currentUserID).child("group" + i).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.getValue().equals(currentGroupName)&& created.compareAndSet(false, true)) {
                            rootRef.child(currentUserID).child("group"+i1).removeValue();
                            Toast.makeText(GroupChatActivity.this, "You left the group. ", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        // Xử lý khi có lỗi xảy ra
                    }
                });

                if (created.get()) {
                    break;
                }
            }


        }
        if (item.getItemId() == R.id.change_background) {

            changeBackground();
        }
        return true;
    }
    private void sendMessage()
    {
        String messageText = messageInputText.getText().toString();

        if(TextUtils.isEmpty(messageText))
        {
            Toast.makeText(this, "Write your message first.", Toast.LENGTH_SHORT).show();
        }
        else
        {

            //Intent chatIntent = new Intent(GroupChat1Activity.this, MainActivity.class);
            //chatIntent.putExtra("last message",messageText );


            String messageSenderRef="Groups/"+currentGroupName+"/Message" ;

            DatabaseReference userMessageKeyRef = rootRef.child("Group")
                    .child(currentGroupName).child("Message").push();

            String messagePushID = userMessageKeyRef.getKey();

            Map messageTextBody = new HashMap<>();
            messageTextBody.put("message",messageText);
            messageTextBody.put("type","text");
            messageTextBody.put("from",currentUserID);
            messageTextBody.put("group", currentGroupName);
            messageTextBody.put("messageID",messagePushID);
            messageTextBody.put("time",saveCurrentTime);
            messageTextBody.put("date",saveCurrentDate);

            Map messageBodyDetail = new HashMap();
            messageBodyDetail.put(messageSenderRef+"/"+ messagePushID, messageTextBody);

            rootRef.updateChildren(messageBodyDetail).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful())
                    {

                    }
                    else
                    {
                        Toast.makeText(GroupChatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                    messageInputText.setText("");
                }
            });

        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        messageList.clear();
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateUserStatus("offline");
        messageList.clear();
    }
    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        rootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 438 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            loadingBar.setTitle("Uploading File");
            loadingBar.setMessage("Please wait while uploading the file");
            loadingBar.setCanceledOnTouchOutside(false);
            loadingBar.show();

            fileUri = data.getData();
            if(!checker.equals("image"))
            {
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Document Files");

                String messageSenderRef="Groups/"+currentGroupName+"/Message";


                DatabaseReference userMessageKeyRef = rootRef.child("Groups")
                        .child(currentGroupName).child("Message").push();


                final String messagePushID = userMessageKeyRef.getKey();

                StorageReference filePath = storageReference.child(messagePushID+"."+ checker);
                filePath.putFile(fileUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task)
                    {
                        if(task.isSuccessful()){
                            task.getResult().getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri downloadUri) {
                                    Map messageImageBody = new HashMap();
                                    messageImageBody.put("message", downloadUri.toString());
                                    messageImageBody.put("name", fileUri.getLastPathSegment());
                                    messageImageBody.put("type", checker);
                                    messageImageBody.put("from", currentUserID);
                                    messageImageBody.put("group", currentGroupName);
                                    messageImageBody.put("messageID", messagePushID);
                                    messageImageBody.put("time", saveCurrentTime);
                                    messageImageBody.put("date", saveCurrentDate);

                                    Map messageBodyDetails = new HashMap();
                                    messageBodyDetails.put(messageSenderRef + "/" + messagePushID, messageImageBody);


                                    rootRef.updateChildren(messageBodyDetails);
                                    loadingBar.dismiss();



                                }
                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingBar.dismiss();
                        Toast.makeText(GroupChatActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                        double p =(100.0*snapshot.getBytesTransferred())/ snapshot.getTotalByteCount();
                        loadingBar.setMessage("Uploading "+(int) p+"%");
                    }
                });


            }
            else if(checker.equals("image"))
            {
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Image Files");

                String messageSenderRef="Groups/"+currentGroupName+"/Message";


                DatabaseReference userMessageKeyRef = rootRef.child("Groups")
                        .child(currentGroupName).child("Message").push();

                final String messagePushID = userMessageKeyRef.getKey();

                StorageReference filePath = storageReference.child(messagePushID+"."+ "jpg");

                uploadTask = filePath.putFile(fileUri);
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if(task.isSuccessful())
                        {
                            Uri downloadUrl = task.getResult();
                            myUrl = downloadUrl.toString();

                            Map messageImageBody = new HashMap();
                            messageImageBody.put("message", myUrl);
                            messageImageBody.put("name", fileUri.getLastPathSegment());
                            messageImageBody.put("type", checker);
                            messageImageBody.put("from", currentUserID);
                            messageImageBody.put("group", currentGroupName);
                            messageImageBody.put("messageID", messagePushID);
                            messageImageBody.put("time", saveCurrentTime);
                            messageImageBody.put("date", saveCurrentDate);

                            Map messageBodyDetails = new HashMap();
                            messageBodyDetails.put(messageSenderRef + "/" + messagePushID, messageImageBody);
                            rootRef.updateChildren(messageBodyDetails);
                            loadingBar.dismiss();



                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingBar.dismiss();
                        Toast.makeText(GroupChatActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            else {
                loadingBar.dismiss();
                Toast.makeText(this, "No image file secleted. Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void changeBackground() {
        CharSequence options[] = new CharSequence[]
                {
                        "Winter",
                        "Learning",
                        "Girls",
                        "Love",
                        "Gaming"
                };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose the background:");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (which == 0) {
                    mRelative.setBackgroundResource(R.drawable.background_winter);
                    rootRef.child("Groups").child(currentGroupName).child("background").setValue("winter");
                    Toast.makeText(GroupChatActivity.this, "Change background successfully!", Toast.LENGTH_SHORT).show();
                }
                if (which == 1) {
                    mRelative.setBackgroundResource(R.drawable.background_learn);
                    rootRef.child("Groups").child(currentGroupName).child("background").setValue("learn");
                    Toast.makeText(GroupChatActivity.this, "Change background successfully!", Toast.LENGTH_SHORT).show();

                }
                if (which == 2) {
                    mRelative.setBackgroundResource(R.drawable.background_girl);
                    rootRef.child("Groups").child(currentGroupName).child("background").setValue("girl");
                    Toast.makeText(GroupChatActivity.this, "Change background successfully!", Toast.LENGTH_SHORT).show();
                }
                if (which == 3) {
                    mRelative.setBackgroundResource(R.drawable.background_love);
                    rootRef.child("Groups").child(currentGroupName).child("background").setValue("love");
                    Toast.makeText(GroupChatActivity.this, "Change background successfully!", Toast.LENGTH_SHORT).show();

                }
                if (which == 4) {
                    mRelative.setBackgroundResource(R.drawable.background_game);
                    rootRef.child("Groups").child(currentGroupName).child("background").setValue("game");
                    Toast.makeText(GroupChatActivity.this, "Change background successfully!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.show();
    }
    private void deleteGroup() {
        rootRef.child("Groups").child("Create by").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String value = snapshot.getValue(String.class);
                if(value.equals(currentUserID)){
                    rootRef.child("Groups").child(currentGroupName.toString())
                            .removeValue()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(GroupChatActivity.this, "Delete this group successfully...", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(GroupChatActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                }
                else{
                    Toast.makeText(GroupChatActivity.this, "Only user create the group can delete it.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}