package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.melychat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class AdminAddUserActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private DatabaseReference rootRef;
    private FirebaseAuth mAuth;
    private Button btAdd;
    private EditText edEmail, edPassword, edUsername, edStatus, edBirthday;
    private ProgressDialog loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_user);
        toolbar = (Toolbar) findViewById(R.id.toolbar_add_user_activity);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setTitle("Add New User");
        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

        Init();
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateNewAccount();
            }
        });

    }

    private void Init() {
        edEmail = (EditText) findViewById(R.id.email_add_user);
        edPassword = (EditText) findViewById(R.id.password_add_user);
        edUsername = (EditText) findViewById(R.id.name_add_user);
        edStatus = (EditText) findViewById(R.id.status_add_user);
        edBirthday = (EditText) findViewById(R.id.birthday_add_user);
        btAdd = (Button) findViewById(R.id.bt_add_user);
        loadingBar = new ProgressDialog(this);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    private void CreateNewAccount() {
        String email = edEmail.getText().toString();
        String password = edPassword.getText().toString();
        String username = edUsername.getText().toString();
        String status = edStatus.getText().toString();
        String birthday = edBirthday.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter the email...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter the password...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, "Please enter the username...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(status)) {
            Toast.makeText(this, "Please enter the status...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(birthday)) {
            Toast.makeText(this, "Please enter the birthday...", Toast.LENGTH_SHORT).show();
        } else {

            loadingBar.setTitle("Create New Account");
            loadingBar.setMessage("Please wait, while creating a new account");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();


            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {

                        String currentUserID1 = mAuth.getCurrentUser().getUid();
                        HashMap<String,Object> profileMap = new HashMap<>();
                        profileMap.put("uid", currentUserID1);
                        profileMap.put("name", username);
                        profileMap.put("status", status);
                        profileMap.put("birthday", birthday);
                        rootRef.child("Users").child(currentUserID1).updateChildren(profileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(AdminAddUserActivity.this, "Create new user successfully...", Toast.LENGTH_SHORT).show();
                                    loadingBar.dismiss();
                                    //Intent intent = new Intent(AdminAddUserActivity.this,AdminActivity.class);
                                    //startActivity(intent);
                                    onBackPressed();
                                }
                                else{
                                    String message = task.getException().toString();
                                    Toast.makeText(AdminAddUserActivity.this, "Error... " +message, Toast.LENGTH_SHORT).show();
                                    loadingBar.dismiss();
                                }
                            }
                        });


                    }
                }
            });
        }
    }
}