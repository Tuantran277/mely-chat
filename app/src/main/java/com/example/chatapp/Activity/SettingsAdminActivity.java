package com.example.chatapp.Activity;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.melychat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsAdminActivity extends AppCompatActivity {

    private Button updateAccountSettings;
    private EditText userName, userStatus, userBirthday;
    private CircleImageView userProfileImage;
    private String userID;

    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    private ActivityResultLauncher<String> galleryLauncher;
    private StorageReference UserProfileImages;
    private ProgressDialog loadingbar;
    private Toolbar editToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_admin);

        rootRef = FirebaseDatabase.getInstance().getReference();
        userID = getIntent().getStringExtra("visit_user_id");
        UserProfileImages= FirebaseStorage.getInstance().getReference().child("Profile Images");
        Init();
        setSupportActionBar(editToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setTitle("Edit User");

        RetrieveUserInfo();

        updateAccountSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateSettings();
                SendUserAdminActivity();
            }
        });
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri result) {
                        if (result != null) {
                            // Xử lý kết quả khi người dùng đã chọn ảnh từ gallery
                            loadingbar.setTitle("Set profile image");
                            loadingbar.setMessage("Please wait a moment while the image was uploading in the database..");
                            loadingbar.setCanceledOnTouchOutside(false);
                            loadingbar.show();

                            userProfileImage.setImageURI(result);
                            /*CropImage.activity(result)
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(this,cropImageResult);*/


                            StorageReference filePath = UserProfileImages.child(userID +".jpg");



                            filePath.putFile(result).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(SettingsAdminActivity.this, "Profile image update successfully!", Toast.LENGTH_SHORT).show();


                                        task.getResult().getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                String downloadUrl = uri.toString();
                                                // xử lý downloadUrl ở đây

                                                rootRef.child("Users").child(userID).child("image")
                                                        .setValue(downloadUrl)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()){
                                                                    Toast.makeText(SettingsAdminActivity.this, "Image save in database, sucessfully...", Toast.LENGTH_SHORT).show();
                                                                    loadingbar.dismiss();
                                                                    userProfileImage.setImageURI(uri);
                                                                }
                                                                else {
                                                                    String message= task.getException().toString();
                                                                    loadingbar.dismiss();
                                                                    Toast.makeText(SettingsAdminActivity.this, "Error: "+ message, Toast.LENGTH_SHORT).show();
                                                                }
                                                            }

                                                        });
                                            }
                                        });
                                    }

                                    else{
                                        String message= task.getException().toString();
                                        Toast.makeText(SettingsAdminActivity.this, "Error"+message, Toast.LENGTH_SHORT).show();
                                        loadingbar.dismiss();
                                    }
                                }
                            });

                        }
                    }
                });


        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                galleryLauncher.launch("image/*");

            }
        });

    }




    private void Init() {
        updateAccountSettings = (Button) findViewById(R.id.update_settings_button);
        userName = (EditText) findViewById(R.id.set_user_name_admin);
        userStatus = (EditText) findViewById(R.id.set_profile_status_admin);
        userProfileImage = (CircleImageView) findViewById(R.id.set_profile_image_admin);
        loadingbar= new ProgressDialog(this);
        editToolbar = (Toolbar) findViewById(R.id.setting_toolbar_admin);
        userBirthday= (EditText) findViewById(R.id.set_profile_birthday_admin);
    }
    private void RetrieveUserInfo()
    {
        rootRef.child("Users").child(userID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name") && (dataSnapshot.hasChild("image"))))
                        {

                            String retrieveProfileImage = dataSnapshot.child("image").getValue().toString();
                            Picasso.get().load(retrieveProfileImage).into(userProfileImage);
                        }

                        String retrieveUserBirthday= dataSnapshot.child("birthday").getValue().toString();
                        String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                        String retrievesStatus = dataSnapshot.child("status").getValue().toString();

                        userName.setText(retrieveUserName);
                        userStatus.setText(retrievesStatus);
                        userBirthday.setText(retrieveUserBirthday);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    private void UpdateSettings() {
        String setUserName = userName.getText().toString();
        String setStatus = userStatus.getText().toString();
        String setBirthday = userBirthday.getText().toString();
        if(TextUtils.isEmpty(setUserName)){
            Toast.makeText(this, "Please enter username first...", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(setStatus)){
            Toast.makeText(this, "Please enter status...", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(setBirthday)){
            Toast.makeText(this, "Please enter birthday...", Toast.LENGTH_SHORT).show();
        }
        else{
            HashMap<String,Object> profileMap = new HashMap<>();
            profileMap.put("uid", userID);
            profileMap.put("name", setUserName);
            //profileMap.put("image",currentImageUri);
            profileMap.put("status", setStatus);
            profileMap.put("birthday", setBirthday);
            rootRef.child("Users").child(userID).updateChildren(profileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(SettingsAdminActivity.this, "Update profile successfully...", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        String message = task.getException().toString();
                        Toast.makeText(SettingsAdminActivity.this, "Error..." +message, Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }

    }
    private void SendUserAdminActivity() {
        Intent mainIntent = new Intent(SettingsAdminActivity.this, AdminActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}