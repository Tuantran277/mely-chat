package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.chatapp.Model.Contacts;
import com.example.melychat.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DisplayMemberGroupActivity extends AppCompatActivity {
    private DatabaseReference usersRef,rootRef;
    private FirebaseAuth mAuth;
    private RecyclerView displayMember;
    private String currentUserID,currentGroupName;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_member_group);

        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        displayMember =(RecyclerView) findViewById(R.id.display_member_recycler_list);
        displayMember.setLayoutManager(new LinearLayoutManager(this));
        currentGroupName = getIntent().getStringExtra("current_group");

        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();
        currentUserID = mAuth.getCurrentUser().getUid();
        mToolbar= (Toolbar) findViewById(R.id.display_member_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Member");


    }





    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser currentUser1 = mAuth.getCurrentUser();
        if(currentUser1 != null)
            updateUserStatus("offline");
    }

    @Override
    protected void onStart() {
        updateUserStatus("online");
        super.onStart();


        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        for (int i = 1; i < 100; i++) {
            usersRef.child("group" + i).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                        String userID = userSnapshot.getKey();
                        // Thực hiện các thao tác với người dùng có ID là userID
                        if (dataSnapshot.exists() && dataSnapshot.equals(currentGroupName)) {
                            for (int i = 1; i < 100; i++) {
                                Query query = usersRef.orderByChild("group" + i).equalTo(currentGroupName);

                                FirebaseRecyclerOptions<Contacts> options =
                                        new FirebaseRecyclerOptions.Builder<Contacts>()
                                                .setQuery(query, Contacts.class)
                                                .build();
                                FirebaseRecyclerAdapter<Contacts, DisplayMemberGroupActivity.DisplayViewHolder> adapter =
                                        new FirebaseRecyclerAdapter<Contacts, DisplayViewHolder>(options) {
                                            @Override
                                            protected void onBindViewHolder(@NonNull DisplayMemberGroupActivity.DisplayViewHolder holder, int position, @NonNull Contacts model) {
                                                holder.userName.setText(model.getName());
                                                holder.userStatus.setText(model.getStatus());
                                                Picasso.get().load(model.getImage()).placeholder(R.drawable.profile_image).into(holder.profileImage);

                                                holder.itemView.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        String visit_user_id = getRef(holder.getBindingAdapterPosition()).getKey();
                                                        Intent profileIntent= new Intent(DisplayMemberGroupActivity.this,ProfileActivity.class);
                                                        profileIntent.putExtra("visit_user_id", visit_user_id);
                                                        startActivity(profileIntent);

                                                    }
                                                });

                                            }

                                            @NonNull
                                            @Override
                                            public DisplayMemberGroupActivity.DisplayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.users_display_layout,parent,false);
                                                DisplayMemberGroupActivity.DisplayViewHolder viewHolder= new DisplayMemberGroupActivity.DisplayViewHolder(view);
                                                return viewHolder;
                                            }
                                        };
                                displayMember.setAdapter(adapter);
                                adapter.startListening();
                            }
                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Xử lý lỗi khi truy xuất dữ liệu
                }
            });


        }




    }
    public static class DisplayViewHolder extends RecyclerView.ViewHolder
    {
        TextView userName, userStatus;
        CircleImageView profileImage;
        public DisplayViewHolder(@NonNull View itemView) {
            super(itemView);
            userName= itemView.findViewById(R.id.user_profile_name);
            userStatus = itemView.findViewById(R.id.user_status);
            profileImage = itemView.findViewById(R.id.user_profile_image);
        }
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        rootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }




}
