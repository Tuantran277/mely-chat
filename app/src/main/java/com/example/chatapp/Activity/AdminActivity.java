package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chatapp.Model.Contacts;
import com.example.melychat.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdminActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private RecyclerView findUserRecyclerList;
    private DatabaseReference usersRef,rootRef;
    private MenuItem menuItem;
    private FirebaseAuth mAuth;
    private String image, currentUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        findUserRecyclerList =(RecyclerView) findViewById(R.id.find_user_admin_recycler_list);
        findUserRecyclerList.setLayoutManager(new LinearLayoutManager(this));

        mAuth = FirebaseAuth.getInstance();

        currentUserID =getIntent().getStringExtra("current_user_id");

        rootRef = FirebaseDatabase.getInstance().getReference();
        mToolbar= (Toolbar) findViewById(R.id.find_user_admin_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Users");

    }
    @Override
    protected void onStart() {
        super.onStart();
        updateUserStatus("online");

        FirebaseRecyclerOptions<Contacts> options =
                new FirebaseRecyclerOptions.Builder<Contacts>()
                        .setQuery(usersRef, Contacts.class)
                        .build();

        FirebaseRecyclerAdapter<Contacts, AdminActivity.AdminViewHolder> adapter =
                new FirebaseRecyclerAdapter<Contacts, AdminActivity.AdminViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull AdminActivity.AdminViewHolder holder, int position, @NonNull Contacts model) {
                        String userIDs= getRef(position).getKey();
                        usersRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {

                                    if (snapshot.child("userState").hasChild("state")) {
                                        String state = snapshot.child("userState").child("state").getValue().toString();
                                        if (state.equals("online")) {
                                            holder.onlineImage.setVisibility(View.VISIBLE);
                                        }
                                        else if (state.equals("offline")) {
                                            holder.onlineImage.setVisibility(View.INVISIBLE);
                                        }
                                    } else {
                                        holder.onlineImage.setVisibility(View.INVISIBLE);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                        holder.userName.setText(model.getName());
                        holder.userStatus.setText(model.getStatus());
                        Picasso.get().load(model.getImage()).placeholder(R.drawable.profile_image).into(holder.profileImage);

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String visit_user_id = getRef(holder.getBindingAdapterPosition()).getKey();
                                Intent profileIntent= new Intent(AdminActivity.this,SettingsAdminActivity.class);
                                profileIntent.putExtra("visit_user_id", visit_user_id);
                                startActivity(profileIntent);

                            }
                        });
//                        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                            @Override
//                            public boolean onLongClick(View v) {
//                                DeleteUser(userIDs);
//                                return true;
//                            }
//                        });

                    }

                    @NonNull
                    @Override
                    public AdminActivity.AdminViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.users_display_layout,parent,false);
                        AdminActivity.AdminViewHolder viewHolder= new AdminActivity.AdminViewHolder(view);
                        return viewHolder;
                    }
                };
        findUserRecyclerList.setAdapter(adapter);
        adapter.startListening();

    }
    public static class AdminViewHolder extends RecyclerView.ViewHolder
    {
        TextView userName, userStatus;
        ImageView onlineImage;
        CircleImageView profileImage;
        public AdminViewHolder(@NonNull View itemView) {
            super(itemView);
            userName= itemView.findViewById(R.id.user_profile_name);
            userStatus = itemView.findViewById(R.id.user_status);
            profileImage = itemView.findViewById(R.id.user_profile_image);
            onlineImage = itemView.findViewById(R.id.user_online_status);

        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            showKeyboard();
            return true;
        }
        if(id == R.id.action_add_user ){
            Intent intent = new Intent(AdminActivity.this, AdminAddUserActivity.class);
            startActivity(intent);
        }
        if (item.getItemId() == android.R.id.home) {
            BacktoMain();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPause() {
        super.onPause();
        updateUserStatus("offline");
    }

    private void BacktoMain(){
        Intent intent = new Intent(AdminActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        androidx.appcompat.widget.SearchView searchView = (androidx.appcompat.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                performSearch(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void performSearch(String newText) {
        DatabaseReference usersRef1 = FirebaseDatabase.getInstance().getReference().child("Users");

        Query searchQuery = usersRef1.orderByChild("name").startAt(newText).endAt(newText + "\uf8ff");
        searchQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Xử lý kết quả truy vấn

                List<Contacts> searchResults = new ArrayList<>();

                searchResults.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String name = snapshot.child("name").getValue(String.class);
                    String status = snapshot.child("status").getValue(String.class);
                    if(snapshot.hasChild("image"))
                        image = snapshot.child("image").getValue(String.class);
                    else
                        image= "";
                    Contacts contact = new Contacts(name, status, image, snapshot.child("uid").toString());
                    searchResults.add(contact);

                }

                FirebaseRecyclerOptions<Contacts> options =
                        new FirebaseRecyclerOptions.Builder<Contacts>()
                                .setQuery(searchQuery, Contacts.class)
                                .build();
                FirebaseRecyclerAdapter<Contacts, AdminActivity.AdminViewHolder> adapter =
                        new FirebaseRecyclerAdapter<Contacts, AdminActivity.AdminViewHolder>(options) {
                            @Override
                            protected void onBindViewHolder(@NonNull AdminActivity.AdminViewHolder holder, int position, @NonNull Contacts model) {
                                Contacts contact = searchResults.get(position);
                                String userIDs= getRef(position).getKey();
                                usersRef.child(userIDs).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()) {

                                            if (snapshot.child("userState").hasChild("state")) {
                                                String state = snapshot.child("userState").child("state").getValue().toString();
                                                if (state.equals("online")) {
                                                    holder.onlineImage.setVisibility(View.VISIBLE);
                                                }
                                                else if (state.equals("offline")) {
                                                    holder.onlineImage.setVisibility(View.INVISIBLE);
                                                }
                                            } else {
                                                holder.onlineImage.setVisibility(View.INVISIBLE);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                                String name = contact.getName();
                                String status = contact.getStatus();
                                String image = contact.getImage();

                                holder.userName.setText(name);
                                holder.userStatus.setText(status);
                                if (image != "")
                                    Picasso.get().load(image).into(holder.profileImage);
                                else {
                                    Picasso.get().load(R.drawable.profile_image).into(holder.profileImage);
                                }

                                holder.itemView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String visit_user_id = model.getUid();
                                        Intent profileIntent= new Intent(AdminActivity.this, SettingsAdminActivity.class);
                                        profileIntent.putExtra("visit_user_id", visit_user_id);
                                        startActivity(profileIntent);

                                    }
                                });
//                                holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                                    @Override
//                                    public boolean onLongClick(View v) {
//                                        DeleteUser(userIDs);
//                                        return true;
//                                    }
//                                });

                            }

                            @NonNull
                            @Override
                            public AdminActivity.AdminViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.users_display_layout,parent,false);
                                AdminActivity.AdminViewHolder viewHolder = new AdminActivity.AdminViewHolder(view);
                                return viewHolder;
                            }
                        };
                findUserRecyclerList.setAdapter(adapter);
                adapter.startListening();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Xử lý khi truy vấn bị hủy
            }
        });
    }

    private void DeleteUser(String userID) {
        CharSequence option[] = new CharSequence[]{
                "Yes",
                "No"
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
        builder.setTitle("Do you want delete this user");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (which == 0) {
                    deleteNodesWithValueOne(rootRef,userID);
                    rootRef.child("Users").child(userID).removeValue();

                }
            }
        });
        builder.show();

    }

    private void deleteNodesWithValueOne(DatabaseReference rootRef, String userID) {
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot childSnapshot : snapshot.getChildren()) {
                    String value = String.valueOf(childSnapshot.getValue());
                    //Log.e("SSSS", "onDataChange: "+value );
                    //Log.e("VVVV",userID);
                    if ( value.equals(userID)) {
                        childSnapshot.getRef().removeValue(); // Xóa nút có giá trị là userID
                    }
                        deleteNodesWithValueOne(childSnapshot.getRef(),userID); // Duyệt các nút con
                    }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        rootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }

}