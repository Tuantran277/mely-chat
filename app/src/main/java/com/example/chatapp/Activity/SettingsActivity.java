package com.example.chatapp.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.melychat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {
    private Button updateAccountSettings, deleteAccountBtn;
    private EditText userName, userStatus, userBirthday;
    private CircleImageView userProfileImage;
    private String currentUserID,currentUserID1;

    private FirebaseAuth mAuth;
    private DatabaseReference rootRef,UsersRef;
    private ActivityResultLauncher<String> galleryLauncher;
    private StorageReference UserProfileImages;
    private ProgressDialog loadingbar;
    private Toolbar SettingsToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        //currentUserID1 = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        FirebaseStorage storage = FirebaseStorage.getInstance();
        UserProfileImages = storage.getReference().child("Profile Images");


        Init();


        deleteAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[]
                        {
                                "Yes",
                                "No"

                        };
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle("Do you want to reset your account?");

                builder.setItems(options, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            UsersRef.child(currentUserID).child("name").setValue("MelyChat User").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    UsersRef.child(currentUserID).child("status").setValue("User status").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            UsersRef.child(currentUserID).child("image").removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    userProfileImage.setImageResource(R.drawable.profile_image);
                                                    UsersRef.child(currentUserID).child("birthday").setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            Toast.makeText(SettingsActivity.this, "Reset your account successfully...", Toast.LENGTH_SHORT).show();
                                                            userName.setVisibility(View.VISIBLE);
                                                        }
                                                    });


                                                }
                                            });

                                        }
                                    });

                                }
                            });


                        }

                    }
            });
                builder.show();
            }
        });

        setSupportActionBar(SettingsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setTitle("Account Settings");
        userName.setVisibility(View.INVISIBLE);
        deleteAccountBtn.setEnabled(true);
        deleteAccountBtn.setVisibility(View.VISIBLE);

        updateAccountSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateSettings();

            }
        });

        RetrieveUserInfo();
        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
                result -> {
                    if (result != null) {
                        // Xử lý kết quả khi người dùng đã chọn ảnh từ gallery
                        loadingbar.setTitle("Set profile image");
                        loadingbar.setMessage("Please wait a moment while the image was uploading in the database..");
                        loadingbar.setCanceledOnTouchOutside(false);
                        loadingbar.show();

                        userProfileImage.setImageURI(result);
                        /*CropImage.activity(result)
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .start(this,cropImageResult);*/


                        StorageReference filePath = UserProfileImages.child(currentUserID +".jpg");



                        filePath.putFile(result).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(SettingsActivity.this, "Profile image update successfully!", Toast.LENGTH_SHORT).show();


                                    task.getResult().getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            String downloadUrl = uri.toString();
                                            // xử lý downloadUrl ở đây

                                            rootRef.child("Users").child(currentUserID).child("image")
                                                    .setValue(downloadUrl)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                Toast.makeText(SettingsActivity.this, "Image save in database, sucessfully...", Toast.LENGTH_SHORT).show();
                                                                loadingbar.dismiss();
                                                                userProfileImage.setImageURI(uri);
                                                            }
                                                            else {
                                                                String message= task.getException().toString();
                                                                loadingbar.dismiss();
                                                                Toast.makeText(SettingsActivity.this, "Error: "+ message, Toast.LENGTH_SHORT).show();
                                                            }
                                                        }

                                                    });
                                }
                            });
                                }

                                else{
                                    String message= task.getException().toString();
                                    Toast.makeText(SettingsActivity.this, "Error"+message, Toast.LENGTH_SHORT).show();
                                    loadingbar.dismiss();
                                }
                            }
                        });

                    }
                });


        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                galleryLauncher.launch("image/*");

            }
        });

    }


    @Override
    protected void onStart() {
        //updateUserStatus("online");
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void UpdateSettings() {
        String setUserName = userName.getText().toString();
        String setStatus = userStatus.getText().toString();
        String setBirthday = userBirthday.getText().toString();
        String currentImageUri = userProfileImage.toString();
        if(TextUtils.isEmpty(setUserName)){
            Toast.makeText(this, "Please write your username first...", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(setStatus)){
            Toast.makeText(this, "Please write your status...", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(setBirthday)){
            Toast.makeText(this, "Please write your birthday", Toast.LENGTH_SHORT).show();
        }
        else{
            HashMap<String,Object> profileMap = new HashMap<>();
                profileMap.put("uid", currentUserID);
                profileMap.put("name", setUserName);
                //profileMap.put("image",currentImageUri);
                profileMap.put("status", setStatus);
                profileMap.put("birthday", setBirthday);
            rootRef.child("Users").child(currentUserID).updateChildren(profileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        SendUserToMainActivity();
                        Toast.makeText(SettingsActivity.this, "Update profile successfully...", Toast.LENGTH_SHORT).show();

                    }
                    else{
                        String message = task.getException().toString();
                        Toast.makeText(SettingsActivity.this, "Error..." +message, Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }

    }

    private void RetrieveUserInfo()
    {
        rootRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name") && (dataSnapshot.hasChild("image"))))
                        {
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrievesStatus = dataSnapshot.child("status").getValue().toString();
                            String retrieveProfileImage = dataSnapshot.child("image").getValue().toString();
                            String retrieveBirthday =dataSnapshot.child("birthday").getValue().toString();

                            userName.setText(retrieveUserName);
                            userStatus.setText(retrievesStatus);
                            userBirthday.setText(retrieveBirthday);

                            Picasso.get().load(retrieveProfileImage).into(userProfileImage);
                        }

                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name")))
                        {
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrievesStatus = dataSnapshot.child("status").getValue().toString();
                            String retrieveBirthday =dataSnapshot.child("birthday").getValue().toString();

                            userBirthday.setText(retrieveBirthday);
                            userName.setText(retrieveUserName);
                            userStatus.setText(retrievesStatus);
                        }
                        else
                        {
                            userName.setVisibility(View.VISIBLE);
                            deleteAccountBtn.setVisibility(View.INVISIBLE);
                            Toast.makeText(SettingsActivity.this, "Please set & update your profile information...", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(SettingsActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }
    private void Init() {
        updateAccountSettings = (Button) findViewById(R.id.update_settings_button);
        userName = (EditText) findViewById(R.id.set_user_name);
        userStatus = (EditText) findViewById(R.id.set_profile_status);
        userProfileImage = (CircleImageView) findViewById(R.id.set_profile_image);
        loadingbar= new ProgressDialog(this);
        SettingsToolbar = (Toolbar) findViewById(R.id.setting_toolbar);
        deleteAccountBtn =(Button) findViewById(R.id.delete_account);
        userBirthday =(EditText) findViewById(R.id.set_birthday);


    }

}