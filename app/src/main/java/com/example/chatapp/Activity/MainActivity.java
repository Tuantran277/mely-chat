package com.example.chatapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatapp.Adapter.TabsAccessorAdapter;
import com.example.melychat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager myViewPager;
    private TabLayout myTabLayout;
    private TabsAccessorAdapter myTabsAccessorAdapter;

    private FirebaseAuth mAuth;
    private DatabaseReference rootRef, groupRef, userRef;
    private String currentUserID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("OPEN_ACTIVITY")) {

                Intent newIntent = new Intent(this, MainActivity.class);
                startActivity(newIntent);

            }
        }
        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        groupRef = FirebaseDatabase.getInstance().getReference().child("Groups");
        toolbar = findViewById(R.id.main_page_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("MelyChat");

        myViewPager = (ViewPager) findViewById(R.id.main_tabs_pager);
        myTabsAccessorAdapter = new TabsAccessorAdapter(getSupportFragmentManager());
        myViewPager.setAdapter(myTabsAccessorAdapter);

        myTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        myTabLayout.setupWithViewPager(myViewPager);
        myTabLayout.getTabAt(0).setIcon(R.drawable.chat);
        myTabLayout.getTabAt(1).setIcon(R.drawable.group);
        myTabLayout.getTabAt(2).setIcon(R.drawable.contacts);
        myTabLayout.getTabAt(3).setIcon(R.drawable.request);
        //myTabLayout.getTabAt(4).setIcon(R.drawable.notification);

        FirebaseUser currentUser = mAuth.getCurrentUser();



    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser1 = mAuth.getCurrentUser();
        if(currentUser1 == null){
            SendUserToLoginActivity();
        }
        else
        {
            currentUserID =mAuth.getCurrentUser().getUid();
            VerifyUserExistance();
            updateUserStatus("online");
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        super.onPause();

        FirebaseUser currentUser1 = mAuth.getCurrentUser();
        if(currentUser1 != null)
            updateUserStatus("offline");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void VerifyUserExistance() {
        String currentUserID = mAuth.getCurrentUser().getUid();
        rootRef.child("Users").child(currentUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.child("name").exists()){
                    //Toast.makeText(MainActivity.this, " Welcome ", Toast.LENGTH_SHORT).show();
                }else{
                    SendUserToSettingsActivity();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;

    }
    //Menu option
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.main_logout_option){

            updateUserStatus("offline");
            mAuth.signOut();
            SendUserToLoginActivity();
            userRef.child(currentUserID).child("device_token").child("result").removeValue();
        }
        if(item.getItemId() == R.id.main_create_group_option){

            RequestNewGroup();
        }
        if(item.getItemId() == R.id.main_edit_user_option){

            SendToEditUser();
        }
        if(item.getItemId() == R.id.main_settings_option){

            SendUserToSettingsActivity();
        }
        if(item.getItemId() == R.id.main_find_friends_option){

            SendUserToFindFriendActivity();
        }
        return true;
    }

    private void SendToEditUser() {
        String currentUserID = mAuth.getCurrentUser().getUid();
        rootRef.child("Users").child(currentUserID).child("role").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    String role = snapshot.getValue(String.class);
                    if (role.equals("1")) {
                        Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                        intent.putExtra("current_user_id", currentUserID);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "This feature is only for admin.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(MainActivity.this, "This feature is only for admin.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    //tao group
    private void RequestNewGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialog);
        builder.setTitle("Enter group name: ");

        final EditText groupNameField = new EditText(MainActivity.this);
        groupNameField.setHint(" ex: Football Team");
        builder.setView(groupNameField);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String groupName = groupNameField.getText().toString();

                if(TextUtils.isEmpty(groupName)){
                    Toast.makeText(MainActivity.this, "Please enter group name... ", Toast.LENGTH_SHORT).show();
                }else{
                    CreateNewGroup(groupName);
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        builder.show();
    }

    private void CreateNewGroup(String groupName) {
       groupRef.child(groupName).addListenerForSingleValueEvent(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot snapshot) {
               if (snapshot.exists()) {
                   Toast.makeText(MainActivity.this, "Group name is exist. Please set othter name", Toast.LENGTH_SHORT).show();
               } else {
                   rootRef.child("Groups").child(groupName).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           if (task.isSuccessful()) {
                               final AtomicBoolean created = new AtomicBoolean(false);
                               for (int i = 1; i < 1000; i++) {
                                   int i1 = i;
                                   rootRef.child("Users").child(currentUserID).child("group" + i).addListenerForSingleValueEvent(new ValueEventListener() {
                                       @Override
                                       public void onDataChange(@NonNull DataSnapshot snapshot) {
                                           if (!snapshot.exists() && created.compareAndSet(false, true)) {
                                               rootRef.child("Users").child(currentUserID).child("group" + i1).setValue(groupName);
                                           }
                                       }

                                       @Override
                                       public void onCancelled(@NonNull DatabaseError error) {
                                           // Xử lý khi có lỗi xảy ra
                                       }
                                   });

                                   if (created.get()) {
                                       break;
                                   }
                               }


                           }


                       }
                       });


                   rootRef.child("Groups").child(groupName).child("Create by").setValue(currentUserID).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           if (task.isSuccessful()) {

                           }
                       }
                   });
                   rootRef.child("Groups").child(groupName).child("background").setValue("default").addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           if (task.isSuccessful()) {

                           }
                       }
                   });
               }
           }
           @Override
           public void onCancelled(@NonNull DatabaseError error) {

           }
       });

    }

    private void SendUserToLoginActivity() {
        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }
    private void SendUserToSettingsActivity() {
        Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
        //settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(settingsIntent);
        //finish();
    }
    private void SendUserToFindFriendActivity() {
        Intent findFriendIntent = new Intent(MainActivity.this, FindFriendsActivity.class);
        startActivity(findFriendIntent);

    }

    private void updateUserStatus(String state){
        String saveCurrentTime, saveCurrentDate;
        Calendar calendar =Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        saveCurrentDate =currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm");
        saveCurrentTime =currentTime.format(calendar.getTime());


        HashMap<String,Object> onlineState = new HashMap<>();
        onlineState.put("time",saveCurrentTime);
        onlineState.put("date",saveCurrentDate);
        onlineState.put("state",state);

        currentUserID = mAuth.getCurrentUser().getUid();
        rootRef.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineState);
    }

}