package com.example.chatapp;

import android.content.Context;
import android.os.StrictMode;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.chatapp.PushNotificationService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FCMSend {
    private static String BASE_URL = "https://fcm.googleapis.com/fcm/send";

    private static String SERVER_KEY = "AAAAx-MymEo:APA91bERPLl9ZuJPMa36aOws34I-L0aikGBlp7RJAZytXDEf2QiG_9O_6tm37F5O-0tuHLp92ndevAAR8IJlTalaW9HU2EzBVkZgmDPrviYy-dduB_ruFmoQ8Hv_oXwk7cm61MCgAKwB";

    public static void pushNotification(Context context, String token, String title, String message){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        RequestQueue queue = Volley.newRequestQueue(context);

        try {
            JSONObject object = new JSONObject();
            object.put("to",token);
            JSONObject notification = new JSONObject();
            notification.put("title", title);
            notification.put("body",message);
            object.put("notification",notification);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL,object , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    System.out.println("FCM"+response);
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "key=" + SERVER_KEY);

                    return params;
                }
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("click_action", "OPEN_ACTIVITY");

                    return params;
                }
            };



            queue.add(jsonObjectRequest);

        }catch (JSONException e){
            e.printStackTrace();

        }



    }

}
